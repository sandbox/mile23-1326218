UUID Field Module: A text field which validates as a UUID.
===


What's a UUID?
---

This: http://en.wikipedia.org/wiki/Universally_unique_identifier#Definition

A UUID is a string of text that looks like this:

	8E32484C-E3C2-488C-9B4F-44EF35831F57


What is this module?
---

UUID Field is a module to add a text field to fieldable entities.

Why not just use the normal core text field? UUID Field will validate that the text the user enters is a UUID.

Note that this module has nothing to do with the UUID module, other than the fact that the letters U, I, and D are in the name. https://www.drupal.org/project/uuid

INSTALLATION:
---

This Drupal 7 module installs like any standard module. Some instructions here: https://drupal.org/documentation/install/modules-themes/modules-7

Place it in your sites/all/modules folder, or anywhere you can put modules.

Enable the module.

You're done.


USE:
---

You can attach a UUID field to any fieldable entity in Drupal.

Documentation on this process here:
  https://drupal.org/documentation/modules/field-ui


WHERE:
---

The code for this project lives here: https://drupal.org/sandbox/Mile23/1326218

You can find instructions for getting its git repo here:
https://drupal.org/project/1326218/git-instructions

Did you find something broken or needing attention? Make an issue in the issue queue: https://drupal.org/project/issues/1326218?categories=All
